require('dotenv').config()

const mysql     = require('mysql')
const connectDB = mysql.createConnection({
    host     : process.env.HOST_DATABASE,
    user     : process.env.USER_DATABASE,
    password : process.env.PASSWORD_DATABASE,
    database : process.env.DATABASE
})

connectDB.connect(function(err) {
    if(err) return console.log(`Error al conectar con Mysql: ${err}`)

    console.log('Conectado correctamente con Mysql')
})

module.exports = connectDB