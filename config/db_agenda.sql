CREATE DATABASE db_agenda;

USE db_agenda;

CREATE TABLE users (
    id          VARCHAR(50) NOT NULL,
    displayName VARCHAR(25) NOT NULL,
    email       VARCHAR(30) NOT NULL UNIQUE,
    pass        VARCHAR(65) NOT NULL,
    PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE contacts (
    id       VARCHAR(50) NOT NULL,
    names    VARCHAR(25) NOT NULL,
    lastName VARCHAR(35),
    email    VARCHAR(30),
    phone    VARCHAR(15) NOT NULL,
    idUser   VARCHAR(50) NOT NULL,
    FOREIGN KEY(idUser) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;