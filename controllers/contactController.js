const contactModel = require('../models/contactModel')
const validate     = require('../validators/contactVal')

exports.createContact = async function(req, res) {
    let respuesta = await validate.validateCreateContact(req.body)

    if(respuesta.error) return res.json(respuesta)

    await contactModel.createContact(req.body)

    res.json(respuesta)
}

exports.updateContact = async function(req, res) {
    let id = req.params.id

    let respuesta = await validate.validateCreateContact(req.body)

    if(respuesta.error) return res.json(respuesta)

    await contactModel.updateContact(id, req.body)

    res.json(respuesta)
}

exports.getContacts = async function(req, res) {
    let contact = await contactModel.getContacts(req.body)

    res.json(contact)
}

exports.deleteContact = async function(req, res) {
    await contactModel.deleteContact(req.body)

    res.json({error: false, msg: "Eliminado correctamente"})
}