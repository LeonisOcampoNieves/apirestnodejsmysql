const userModel = require('../models/userModel')
const validate  = require('../validators/userVal')
const auth      = require('../middleware/auth')

exports.getUserById = async function(req, res) {
    let id = req.params.id

    let user = await userModel.getUserById(id)

    res.json(user)
}

exports.createUser = async function(req, res) {
    let respuesta = await validate.validateCreateUser(req.body)

    if(respuesta.error) return res.json(respuesta)

    await userModel.createUser(req.body)

    res.json(respuesta)
}

exports.updateUser = async function(req, res) {
    let id = req.params.id

    let respuesta = await validate.validateUpdateUser(id, req.body)

    if(respuesta.error) return res.json(respuesta)

    await userModel.updateUser(id, req.body)

    res.json(respuesta)
}

exports.loginUser = async function(req, res) {
    let respuesta = await validate.validateLoginUser(req.body)

    if(respuesta.error) return res.json(respuesta)

    let user = respuesta.user

    respuesta = {
        error: false,
        user: {
            idUser : user.id,
            name   : user.displayName
        },
        token: auth.createToken(user)
    }

    res.json(respuesta)
}