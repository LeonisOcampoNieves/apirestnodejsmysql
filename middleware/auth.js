require('dotenv').config()

const jwt    = require('jwt-simple')
const moment = require('moment')

// Validar si el token está activo
exports.isAuth = function(req, res, next) {
    if(!req.headers.authorization){
        return res.status(403).send({message: 'La peticion no tiene la cabecera de autenticación'});
    } else {
        var token = req.headers.authorization.split(" ")[1];
        
        try{
            var payload = jwt.decode(token, process.env.SECRET_TOKEN);

            if(payload.exp <= moment().unix()){
                return res.status(401).send({
                    message: 'EL token ha expirado'
                });
            }
        } catch (ex){
            return res.status(404).send({
                message: 'EL token no es valido'
            });
        }

        req.user = payload;
        
        next();
    } 
}

// Creación de token
exports.createToken = function(user) {
    const payload = {
        sub: user.id,
        ait: moment().unix(),
        exp: moment().add(14, 'days').unix()
    }

    return jwt.encode(payload, process.env.SECRET_TOKEN)
}