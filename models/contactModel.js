const connectDB = require('../config/database')
const uuidv1    = require('uuid/v1')

exports.createContact = async function(body) {
    let promise = new Promise((resolve, reject) => {
        let idContact = uuidv1()
        
        let strSql = "INSERT INTO contacts" +
        "(id,names,lastName,email,phone,idUser) " +
        "VALUES (" +
        "'" + idContact     + "'," +
        "'" + body.names    + "'," +
        "'" + body.lastName + "'," +
        "'" + body.email    + "'," +
        "'" + body.phone    + "'," +
        "'" + body.idUser   + "'" +
        ")"

        connectDB.query(strSql,(err, rows, fields) => {
            resolve(idContact)
        })
    })

    let result = await promise

    return result
}

exports.updateContact = async function(id, body) {
    let promise = new Promise((resolve, reject) => {
        let strSql = "UPDATE contacts SET " +
        "names =    '" + body.names + "'," +
        "lastName = '" + body.lastName + "'," +
        "email =    '" + body.email + "'," +
        "phone =    '" + body.phone + "' " +
        "WHERE " +
        "id = '" + id + "' "
 
        connectDB.query(strSql,(err, rows, fields) => {
            resolve(1)
        })
    })

    let result = await promise

    return result
}

exports.getContactById = async function(id) {

}

exports.getContacts = async function(body) {
    let promise = new Promise((resolve, reject) => {
        let strSql = "SELECT names,lastName,email,phone FROM contacts WHERE idUser =  '" + body.idUser + "' "

        connectDB.query(strSql,(err, rows, fields) => {
            resolve(rows)
        })
    })

    let result = await promise

    return result
}

exports.deleteContact = async function(body) {
    let promise = new Promise((resolve, reject) => {
        let strSql = "DELETE FROM contacts " +
        "WHERE " +
        "id = '" + body.id + "' "

        connectDB.query(strSql,(err, rows, fields) => {
            resolve(1)
        })
    })

    let result = await promise

    return result
}