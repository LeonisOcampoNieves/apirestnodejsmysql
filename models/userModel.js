const connectDB = require('../config/database')
const uuidv1    = require('uuid/v1')
const bcryptjs  = require('bcryptjs')

exports.createUser = async function(body) {
    // Encripto la contraseña
    body.pass = await this.encryptPassword(body.pass)

    let promise = new Promise((resolve, reject) => {
        let idUser = uuidv1()

        let strSql = "INSERT INTO users" +
        "(id,displayName,email,pass) " +
        "VALUES (" +
        "'" + idUser           + "'," +
        "'" + body.displayName + "'," +
        "'" + body.email       + "'," +
        "'" + body.pass        + "'" +
        ")"

        connectDB.query(strSql,(err, rows, fields) => {
            resolve(idUser)
        })
    })

    let result = await promise

    return result
}

exports.updateUser = async function(id, body) {
    let promise = new Promise((resolve, reject) => {
        let strSql = "UPDATE users SET " +
        "displayName = '" + body.displayName + "'," +
        "email = '" + body.email + "' " +
        "WHERE " +
        "id = '" + id + "' "

        console.log(strSql)
        connectDB.query(strSql,(err, rows, fields) => {
            resolve(1)
        })

    })

    let result = await promise
    
    return result
}

exports.updatePassUser = async function(id, body) {
    let promise = new Promise((resolve, reject) => {
        let strSql = "UPDATE users SET " +
        "pass = '" + body.pass + "' " +
        "WHERE " +
        "id = '" + id + "' "

        connectDB.query(strSql,(err, rows, fields) => {
            resolve(1)
        })
    })

    let result = await promise

    return result
}

exports.getUserByEmail = async function(email) {
    let promise = new Promise((resolve, reject) => {
        connectDB.query("SELECT * FROM users WHERE email = '" +email + "';",(err, rows, fields) => {
            resolve(rows[0])
        })
    })

    let result = await promise

    return result
}

exports.getUserById = async function(id) {
    let promise = new Promise((resolve, reject) => {
        connectDB.query("SELECT displayName,email FROM users WHERE id = '"+id+"';",(err, rows, fields) => {
            resolve(rows[0])
        })
    })

    let result = await promise

    return result
}

exports.getUserByEmailWithPass = async function(email) {
    let promise = new Promise((resolve, reject) => {
        connectDB.query("SELECT * FROM users WHERE email = '"+email+"';",(err, rows, fields) => {
            resolve(rows[0])
        })
    })

    let result = await promise

    return result
}

// Encriptación de contraseña
exports.encryptPassword = async function(password) {
    /*
        genSalt es una especie de hash que aplica el algoritmo para hacerlo mas seguro.
        cuantas veces queremos aplicar el algoritmo, en este caso es 10 veces.
        genSalt es un metodo asincrono
    */
    const salt = await bcryptjs.genSalt(10)

    /*
        hash es el encargado de convertir un string(password) en una especie de caracteres
        indescifrables, convierte nuestra contraseña en una serie de caracteres dificiles
        de entender
    */
    return bcryptjs.hash(password, salt)
}

// Validación de contraseña
exports.validatePassword = function(dataPassword, userPassword) {
    
    return bcryptjs.compare(dataPassword, userPassword)
}