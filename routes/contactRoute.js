const contactController = require('../controllers/contactController')
const auth              = require('../middleware/auth')

router.post('/createContact',    auth.isAuth, contactController.createContact)
router.put('/updateContact/:id', auth.isAuth, contactController.updateContact)
router.get('/getContacts',       auth.isAuth, contactController.getContacts)
router.delete('/deleteContact',  auth.isAuth, contactController.deleteContact)