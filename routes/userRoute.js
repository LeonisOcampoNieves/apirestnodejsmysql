const userController = require('../controllers/userController')

router.get('/getUserById/:id', userController.getUserById)
router.post('/createUser',     userController.createUser)
router.post('/loginUser',      userController.loginUser)
router.put('/updateUser/:id',  userController.updateUser)