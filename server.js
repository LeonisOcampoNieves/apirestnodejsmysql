const express    = require('express')
const bodyParser = require('body-parser')

// Instanciamos el servidor Express
const app = express()

global.router = express.Router()

require('require-all')(__dirname + '/routes')

// Configuración
app.set('port', process.env.PORT || 3000)

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// Rutas
app.use('/api', router)

app.listen(app.get('port'), () => {
    console.log(`Conectado al servidor correctamente, corriendo en el puerto http://localhost: ${app.get('port')}`)
})