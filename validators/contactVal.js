exports.validateCreateContact = async function(data) {
    if(data.names == undefined) return {Error: true, Msg: "Campo 'Nombre' no encontrado"}
    if(data.phone == undefined) return {Error: true, Msg: "Campo 'Teléfono' no encontrado"}
    if(data.names == "")        return {Error: true, Msg: "Campo 'Nombre' no debe estar vacío"}
    if(data.phone == "")        return {Error: true, Msg: "Campo 'Teléfono' no debe estar vacío"}

    return {Error: false, Msg: "Registrado correctamente"}
}