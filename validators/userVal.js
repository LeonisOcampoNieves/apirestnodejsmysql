const utils     = require('../utils/utils')
const userModel = require('../models/userModel')

exports.validateCreateUser = async function(data) {
    if(data.displayName        == undefined)  return {error: true, msg: "Campo 'Nombre de usuario' no encontrado"}
    if(data.email              == undefined)  return {error: true, msg: "Campo 'Email' no encontrado"}
    if(data.pass               == undefined)  return {error: true, msg: "Campo 'Contraseña' no encontrado"}
    if(data.pass2              == undefined)  return {error: true, msg: "Campo 'Contraseña 2' no encontrado"}
    
    if(data.displayName.trim() == "")         return {error: true, msg: "Debe digitar un nombre de usuario válido"}
    if(data.pass.trim()        == "")         return {error: true, msg: "Debe digitar una contraseña válida"}
    if(data.pass.trim() != data.pass2.trim()) return {error: true, msg: "Las contraseñas no son iguales"}

    if(!utils.validarEmail(data.email.trim())) return {error: true, msg: "El email ingresado no es válido"}
    
    let user = await userModel.getUserByEmail(data.email.trim())

    // Validamos si el email ya está registrado en la base de datos
    if(user != undefined && user.id != data.id) return {error: true, msg: "El correo ya encuentra registrado"}
    
    return {Error: false, Msg: "Registrado correctamente"}
}

exports.validateUpdateUser = async function(id, data) {
    let user = await userModel.getUserById(id)

    if(user             == undefined) return {error: true, msg: "Usuario no válido"}
    if(data.displayName == undefined) return {error: true, msg: "Campo 'Nombre' no encontrado"}
    if(data.email       == undefined) return {error: true, msg: "Campo 'Email' no encontrado"}

    if(data.displayName.trim() == "") return {error: true, msg: "Debe digitar un nombre válido"}
    if(data.email.trim()       == "") return {error: true, msg: "Debe digitar un email válido"}

    if(!utils.validarEmail(data.email.trim())) return {error: true, msg: "El email ingresado no es válido"}

    user = await userModel.getUserByEmail(data.email.trim())

    // Validamos si el email ya está registrado en la base de datos
    if(user != undefined && user.id != id) return {error: true, msg: "El correo ya encuentra registrado"}

    return {error: false, msg: "Modificado correctamente"}
}

exports.validateLoginUser = async function(data) {
    if(data.email        == undefined)         return {error: true, msg: "Campo 'Email' no encontrado"}
    if(data.pass         == undefined)         return {error: true, msg: "Campo 'Contraseña' no encontrado"}
    
    if(data.email.trim() == "")                return {error: true, msg: "Debe digitar una email válido"}
    if(data.pass.trim()  == "")                return {error: true, msg: "Debe digitar una contraseña válida"}
    
    if(!utils.validarEmail(data.email.trim())) return {error: true, msg: "El email ingresado no es válido"}

    let user = await userModel.getUserByEmailWithPass(data.email.trim())
    
    if(user == undefined) return {error: true, msg: "El correo no se encuentra registrado"}

    /* 
        Valido la contraseña encriptada
        se envia como primer parámetro la contraseña ingresada por el usuario(data.pass)
        y como segundo parametro se envia el password de la consulta realizada(user.pass)
    */
    const validatePassword = await userModel.validatePassword(data.pass, user.pass)

    if(!validatePassword) return {error: true, msg: "La contraseña es incorrecta"}

    return {error: false, msg: "Logueado correctamente", user}
}